import dinopy
from math import ceil
from itertools import starmap, chain


class outp(object):
    """This class implements methods, which are 
    required for handling the DNA-sequence output 
    in a .fasta file.
    
    Method names:
    write -- writes the information in the given file
    """


    def __init__(self, file):
        """Initialize the instance variables, all private"""
        self._file = file
        self._output = None
        self._written = False

    def write(self):
        """finales rausschreiben der daten in die datei"""
        if not self._written:
            self._written = True
            with open(self._file, 'w') as handle:
                handle.writelines(self._output)
                return True
        return False
    
    def _prepare_output(self, header, qg_counter, qunique_sub, q):
        """prepare the the sequence information for output.

        Keyword arguments:
        header --
        qg_counter --
        qunique_sub --
        q --
        """
        yield header
        yield ";{}-unique substring:\n".format(q)
        for el in self._format_sequence(qunique_sub):
            yield el
        yield ";{}-grams:occurrence listing:\n".format(q)
        for line in self._translate_qg_counter(qg_counter):
            yield line

    def _format_sequence(self, seq, limit=80):
        """max 80 basen pro reihe"""
        for i in range(0, ceil(len(seq) / limit)):
            yield "{}\n".format(seq[i*limit : (i+1)*limit])

    def _translate_qg_counter(self, qg_counter):
        """(;<number> times:\n<qgram>\n)*
        string format for output
        """
        for qg, freq in qg_counter.items():
            yield ";{} times:\n".format(freq)
            for el in self._format_sequence(qg):
                yield el

    def __call__(self, *out):
        """anhängen der output daten

        Keyword arguments:
        out -- outputliste mit (header, qg_counter, qunique_sub, q)- tuplen/listen
        """
        def mapper(*var):
            if len(var) != 4:
                raise ValueError()
            return self._prepare_output(*var)
        output = chain(*starmap(mapper, out))
        self._output = chain(self._output, output) if self._output else output

def read_fasta(file, repl_strat=lambda x: x):
    """liest von einer fasta-datei header, sequenz-tuple ein
    optional: verarbeitungsstrategie für invalide chars

    Keyword arguments:
    file --
    reple_strat -- (default=lambda x: x)
    """
    far = dinopy.FastaReader(file)
    return ((header, repl_strat(seq.decode("utf-8"))) for seq, header in far.reads(read_names=True))
