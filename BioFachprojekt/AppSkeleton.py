from spyre import server
from functools import reduce
from pandas import DataFrame
from bioinformatics import qgram_counter, generate, qunique_substring
import output as out

class Validator(object):
    def __init__(self):
        self._validatorMap = dict([])

    def bind_to_validator(self, key, validator):
        self._validatorMap[key] = lambda args: validator(args)

    def invalid_keys(self, args, keys = None):
        if not keys:
            keys = self._validatorMap.keys()
        
        keyIsValid = {key:self._validatorMap[key](args) for key in keys}
        return [key for key, ok in keyIsValid.items() if not ok]

    def validate(self, args, keys = None):
        return not self.invalid_keys(args, keys)

    @staticmethod
    def seq_validator(key, obj):
        def checker(params):
            if obj._isRandomMode(params):
                return params[key].isdigit()
            return all((base in "atgcATGC" for base in params[key]))
        
        return checker

    @staticmethod
    def q_validator(key, seqKey, obj):
        def checker(params):
            if params[key] <= 0:
                return False
            if obj._isRandomMode(params):
                try:                   
                    return int(params['seqIn']) >= params[key]
                except Exception:
                    return False
            return len(params.get(seqKey,"")) >= params[key]
        
        return checker


class AppInfo(object):
    def __init__(self):
        self._inputHelp = dict([])

    def help_screen(self, keys = None):
        if not keys:
            keys = self._inputHelp.keys()
            
        helpLines = [self._get_help_line(*self._inputHelp[key]) for key in keys]
        return reduce(lambda ac, line: ac + "<br>" + line, helpLines, "<ul>") + "</ul>"

    def _get_help_line(self, key, help):
        return "<li><b>{}</b> : <i>{}</i>".format(key, help)

    def append_help_screen(self, key, help, friendlyName = None):
        if not friendlyName:
            friendlyName = key
            
        self._inputHelp[key] = (friendlyName, help)

    def error_msg(self, keys):
        friendlyIfExists = (self._inputHelp.get(key, (key, "ignore"))[0] for key in keys)

        invalidArgs = ", ".join(friendlyIfExists)
        return "<p>The following arguments are not valid. Please modify them.<br><i>{}</i></p>".format(invalidArgs)


class App(server.App):
    def __init__(self, title):        
        super().__init__()
        self.title = title
        self._validator = Validator()
        self._appInfo = AppInfo()
        
        self._init_components()


    def _init_components(self):     
        self._mapParaTo = dict([])
        
        self.tabs = ['Info','qGram-Table']
        
        self._init_inputs()
        self._init_outputs()
        self._init_controls()

    def _init_inputs(self):
        self._init_inputMode();
        self._init_seqIn()
        self._init_qIn()
        self._init_opChboxes()

    def _init_inputMode(self):
        dropdownOptions = [
        {"label": "Type in Sequence", "value":"user", "checked":True},
        {"label":"Autogenerate Sequence", "value":"gen"}]
        self.append_input('dropdown', 'inputMode', label = "Switch input mode", action_id = 'result', options = dropdownOptions, on_page_load = False)

    def _init_seqIn(self):
        self.append_input('text', 'seqIn', label = 'enter a sequence', action_id = 'result', on_page_load = False)
        self._validator.bind_to_validator('seqIn', Validator.seq_validator('seqIn', self))
        self._appInfo.append_help_screen('seqIn', 'The sequence that is used for the all the calculations', 'Sequence')
        self.append_mapper('seqIn', lambda s: s.upper())

    def _init_qIn(self):
        self.append_input('slider', 'qIn', label = 'enter a q', value = 4, min = 1, max = 30, action_id = 'result', on_page_load = False)
        self._validator.bind_to_validator('qIn', Validator.q_validator('qIn', 'seqIn', self))
        self._appInfo.append_help_screen('qIn', 'The q (>0 and <=sequence length) that is needed for the qGram creation', 'q')
        self.append_mapper('qIn', lambda q: int(q))

    def _init_opChboxes(self):
        checkOptions = [{"label": "Longest qunique substring", "value":"longest", "checked":True}]
        self.append_input('checkboxgroup', 'opChboxes', label = 'Optionals', action_id = "result", options = checkOptions)
        self._appInfo.append_help_screen('opChboxes', '<li>Get the longest substring of the seqeunce that is still qunique', 'Optionals')

    def _init_outputs(self):
        self.append_output('html', 'infopanel', control_id = 'result', tab = 'Info')
        self.append_output('table', 'qgtable', control_id = 'result', tab = 'qGram-Table')
        self.append_output('download', 'downOut', control_id = 'down_result', on_page_load = False)

    def _init_controls(self):
        self.controls.append({'id':'result', 'type':'hidden'})
        self.controls.append({'id':'down_result', 'type':'button', 'label':'download result'})
        

    def append_input(self, type, key, **attr):
        if not attr:
            attr = dict([])
            
        attr["type"] = type
        attr["key"] = key
        self.inputs.append(attr)

    def append_output(self, type, id, **attr):
        if not attr:
            attr = dict([])
            
        attr["type"] = type
        attr["id"] = id
        self.outputs.append(attr)        
    
    def append_mapper(self, key, mapper):
        self._mapParaTo[key] = mapper
        

    def getHTML(self, params):
        try:
            params = self._map_if_valid(params)
        except Exception:
            return self._get_error_help_msg(params)
        
        return self._transform_output(params)

    def getData(self, params):
        try:
            params = self._map_if_valid(params)
        except Exception:
            return None
     
        qgs = qgram_counter(params['seqIn'], params['qIn'])
        qgCol, freqCol = zip(*qgs.items())

        return DataFrame({'QGram':list(qgCol), 'Frequency':list(freqCol)})

    def getDownload(self, params):
        raise Exception("Not yet implemented")
        #todo
        try:
            params = self._map_if_valid(params)
        except Exception:
            return None

        outprinter = out.outp("result.fasta")
        file.write(str(self.getData(params)))

        q = params['qIn']
        seq = params['seqIn']
        qgs = qgram_counter(seq, q)
        qsub = qunique_substring(seq, q)
            
        outprinter(">auto generated header",qgs, qsub, q)
        #outprinter.write()      
    
    def _map_if_valid(self, params):
        if not self._validator.validate(params):
            raise Exception('invalid args')
        params = self._mapSeqIn(params)
        params = self._map_params(params)
        return params

    def _map_params(self, params):
        return {key:self._mapParaTo.get(key, lambda p: p)(val) for key, val in params.items()}

    def _mapSeqIn(self, params):
        if not self._isRandomMode(params):
            return params
        params['seqIn'] = generate(int(params['seqIn']))
        return params
    

    def _transform_output(self, params):
        header = "<h1>Parameter assignments</h1><br>"
        seqInfo = "<b>"+("Random generated" if self._isRandomMode(params) else "Given ") + " sequence:</b><br>"

        seq = params['seqIn']
        optionals = "<br>"+"<br>".join(self._get_optionals_to_output(params).values())
        return "<p>" + header + seqInfo + seq + optionals + "</p>"

    def _isRandomMode(self, params):
        return params['inputMode'] == "gen"

    def _get_optionals_to_output(self, params):
        optionalsToStr = dict([])
        if 'longest' in params['opChboxes']:
            seq = params['seqIn']
            q = params['qIn']
            optionalsToStr['longest'] = "<b>Longest qunique subsequence:</b><br>" + qunique_substring(seq, q)
        return optionalsToStr

    def _get_error_help_msg(self, params):
        return self._appInfo.error_msg(self._validator.invalid_keys(params)) + "<br>" + self._appInfo.help_screen()
      
        

ap = App("QGram Analyzer")
ap.launch()
        
