import dinopy
import random
from collections import Counter
from functools import reduce

def rev_comp(sequence):
	"""returns the reverse complement of the given sequence
	
	Keyword arguments:
	sequence -- DNA-sequence you want to analyze
	"""
	return dinopy.reverse_complement(sequence)

def check_q_uniqueness(sequence, q):
	"""returns (is_q_unique, index) such that sequence[0:index] is the first q-unique subsequence

	Keyword arguments:
	sequence -- DNA-sequence you want to analyze
	q -- int, which indicates the length of qgrams
	"""
	if len(sequence) < q:
		return (True, 0)
	forbidden_qgrams = set()
	for pos in range(0, len(sequence) - q + 1):
		qgram = sequence[pos: pos + q]
		revcomp_qgram = rev_comp(qgram)
		if qgram == revcomp_qgram or qgram in forbidden_qgrams or revcomp_qgram in forbidden_qgrams:
			return (False, pos+q-1)
		forbidden_qgrams.add(qgram)
		forbidden_qgrams.add(revcomp_qgram)
	return (True, len(sequence))

def qgram_counter(sequence, q):
	"""returns a dictionary with all qgrams and their frequency
	
	Keyword arguments:
	sequence -- DNA-sequence you want to analyze
	q -- int, which indicates the length of qgrams
	"""
	return Counter(dinopy.qgrams(sequence, q))

def generate(length, frequences = {base:1 for base in "ATGC"}):
    bounds = []
    maxbound = 0
    for base, freq in frequences.items():
    	maxbound += freq
    	bounds.append((base, maxbound))
    def get_base(value):
    	for base, bound in bounds:
    		if value < bound:
    			return base
    seq = ""
    for i in range(0, length):
    	seq += get_base(random.uniform(0, maxbound))
    return seq

def qunique_substring(seq, q):
    """computes longest q-unique substring of seq
       wichtig: qgrams(_,_) darf qgrame nicht umsortieren
       also: q kommt vor q' in seq => q kommt vor q' in qgrams

    Keyword arguments:
    seq -- DNA-sequence you want to analyze
    q -- int, which indicates the length of qgrams
    """
    assert len(seq) >= q
    #berechnet den längsten suffix s von cur_sub
    #sodass s + qgram qunique ist
    def lgst_sub(sub, qgram):
        rev = rev_comp(qgram)
        if qgram == rev:
            return []

        index = 0
        while index < len(sub) and rev != sub[-(index + 1)] != qgram:
            index = index + 1
        return sub[len(sub) - index:] + [qgram]

    cur_sub, max_sub = [], []
    #inv für loop i:
    #   max_sub ist längster q_unique substring von seq[0:i]
    #   cur_sub ist q_unique und in seq zusammenhängend
    for qgram in dinopy.qgrams(seq, q):
        cur_sub = lgst_sub(cur_sub, qgram)
        if len(cur_sub) > len(max_sub):
            max_sub = cur_sub

    #berechne sub_string aus max. qgram menge (schneidet "überlappungen" aus)
    if max_sub:
        return reduce(lambda ac, c : ac + c[-1], max_sub[1:], max_sub[0])
    return ""

def strategy(strat = "delete", args = None):
    """definiert drei verarbeitungsstrategien (strat: seq->seq_mod):
       1) delete: löscht invalide chars
       2) replace: ersetzt invalide chars durch den in args übergebenen char
       3) nearest: ersetzt invalide chars durch den nächst-liegensten validen

    Keyword arguments:
    strat -- name of the strategy you want to use (default=delete)
    args -- arguments you want to pass to the choosen strategy (default=None)
    """
    valid_base = ['A', 'T', 'C', 'G', 'a', 't', 'g', 'c']
    def delete(seq):
        return replace(seq, "")
    def replace(seq, fill):
        return (char if char in valid_base else fill for char in seq)
    def nearest(seq):
        for char in seq:
            li = [(ch, abs(ord(ch) - ord(char.upper()))) for ch in ['A', 'T', 'C', 'G']]
            yield min(li, key= lambda x: x[1])[0]
            
    func = lambda s:{"delete":delete(s), "replace":replace(s, args), "nearest":nearest(s)}[strat]
    return lambda s: reduce(lambda ac, c: ac + c.upper(), func(s), "")
