import os.path
import argparse
import dinopy
import bioinformatics as bio
import output
import re


def get_arg_parser():
	"""returns the argument parser"""

	parser = argparse.ArgumentParser()
	parser.add_argument("q", help="specify the q for q-grams and q-uniqueness", type=int)

	subparsers = parser.add_subparsers(dest="command")
	random_parser = subparsers.add_parser("random", help="generate a random sequence")
	random_parser.add_argument("l", help="length of random sequence", type=int)
	random_parser.add_argument("-f", "--frequencies", help="specify frequencies of A, T, C and G (in this order) for random sequence", nargs=4, type=float)
	random_parser.add_argument("-o", "--output_file", help="specify the file to write the results; if left blank, the results are dumpt in terminal")

	qgram_parser = subparsers.add_parser("qgrams", help="write the first q-unique subsequence and all qgrams to a file")
	input_group = qgram_parser.add_mutually_exclusive_group(required=True)
	input_group.add_argument("-i", "--input_file", help="read the input sequence(s) from this .fasta file")
	input_group.add_argument("-s", "--sequence", help="the input sequence")
	input_group.add_argument("-r", "--random", help="use random sequence of specified length as input", type=int)

	qgram_parser.add_argument("--longest", help="get longest q-unique subsequence instead of first", action="store_true")
	qgram_parser.add_argument("-f", "--frequencies", help="specify frequencies of A, T, C and G (in this order) for random sequence", nargs=4, type=float)
	qgram_parser.add_argument("-o", "--output_file", help="specify the file to write the results; if left blank, the results are dumpt in terminal")
	qgram_parser.add_argument("--strategy", help="specify the strategy", choices=["nearest", "delete", "replace_A", "replace_T", "replace_C", "replace_G"])
	return parser

def write_to_file(string, filename):
	"""writes given string to the specified filename
	if overwrite=True filename is overwriten, else filename is ammended

	Keyword arguments:
	string -- information, which should be written to the file
	filename -- name of the file, where to write the information
	"""

	file_mode = "w"
	if os.path.isfile(args.output_file):
		file_mode = ""
		while not ("w" == file_mode or "a" == file_mode):
			file_mode = input("The file exists already. Type 'w' to overwrite the file or 'a' to ammend these results to the file: ")
	with open(filename, file_mode) as output_file:
		output_file.write(string)

args = get_arg_parser().parse_args()

if "random" == args.command:
	output_string = ">random sequence\n"
	sequence = ""
	if args.frequencies:
		frequencies = dict(zip("ATCG", args.frequencies))
		sequence = bio.generate(args.l, frequencies)
	else:
		sequence = bio.generate(args.l)
	output_string += sequence + "\n"
	if not args.output_file:
		print(output_string)
	else:
		write_to_file(output_string, args.output_file)
	is_q_unique = bio.check_q_uniqueness(sequence, args.q)[0]
	print("sequence is {}q unique".format("not " if not is_q_unique else ""))
else:
	sequences = []
	if args.sequence:
	    seq_pat = re.compile("[ATGCatgc]+$")
	    if seq_pat.match(args.sequence) and len(args.sequence) >= args.q:
	        sequences = [(";custom sequence <no header>\n", args.sequence.upper())]
	    else:
	        print("invalid sequence (sequences only containing A,T,C,G and with a length of at least q are allowed)")
	        exit()
	        
	elif args.input_file:
		if args.strategy and "replace" in args.strategy:
			split = args.strategy.split("_")
			sequences = output.read_fasta(args.input_file, bio.strategy(split[0], split[1]))
		elif args.strategy:
			sequences = output.read_fasta(args.input_file, bio.strategy(args.strategy))
		else:
			sequences = output.read_fasta(args.input_file)
	    
	elif args.random:
	    if args.random <= 0:
	        print("cannot create an empty sequence")
	        exit()
	    seq = None
	    if args.frequencies:
	        seq = bio.generate(args.random, dict(zip("ATCG", args.frequencies)))
	    else:
	        seq = bio.generate(args.random)
	    print("Generated sequence: ", seq)
	    sequences = [(";random generated sequence <no header>\n", seq)]
	    
	else:
	    print("please give a sequence as input with -s or -i. you can use a random generated sequence too (see f)")
	    exit()

	def subseq(seq, q):
		if args.longest:
			return bio.qunique_substring(seq, q)
		else:
			return seq[0:bio.check_q_uniqueness(seq, q)[1]]

	if args.output_file:
	    printer = output.outp(args.output_file)
	    printer(*[(head, bio.qgram_counter(seq, args.q), subseq(seq, args.q), args.q) for head, seq in sequences])
	    print("Writing the results to the choosen output file was {}".format("successfull" if printer.write() else "failure"))
	else:    
	    for head, seq in sequences:
	        subseq = subseq(seq, args.q)
	        print("{}-unique subsequence: {}".format(args.q,subseq)) 
