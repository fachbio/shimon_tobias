import re
import pytest
import bioinformatics as bio
from itertools import product

dna_regex = re.compile("[ACGT]*$")

def gen_seq():
    return ["", "A", "AT", "AAA", "ATGC", "GCTAGCTA", "GATTACA"]

def test_qgram_counter():
    assert bio.qgram_counter("", 2) == {}
    assert bio.qgram_counter("ACGT", 2) == {"AC":1, "CG":1, "GT":1}
    assert bio.qgram_counter("AAAA", 3) == {"AAA":2}

@pytest.mark.parametrize("seq", gen_seq())
def test_rev_comp(seq):
    inv = {'A':'T', 'T':'A', 'C':'G', 'G':'C'}
    rev = bio.rev_comp(seq)
    assert len(seq) == len(rev)
    for i in range(0, len(seq)):
        assert inv[seq[i]] == rev[-i-1]

@pytest.mark.parametrize("seq, q, expected", {
						("", 1, (True, 0)), \
						("AGCAGC", 3, (True, 6)), \
						("AGAG", 2, (False, 3)), \
						("ACAGT", 2, (False, 4)), \
						("AGCT", 4, (False, 3))}, \
						ids=[
							"q > len(seq)", "correct", "failure: q-gram", \
                                                	"failure: reverse complement", "failure: self reverse complementary"])
def test_check_q_uniqueness(seq, q, expected):
	assert bio.check_q_uniqueness(seq, q) == expected


@pytest.mark.parametrize("length, freq, times", [(100,None, 500),\
                                                 (50,{"A":0, 'T':0.5, "G":0.1, "C":0.4}, 500)])
def test_generate(length, freq, times):
    for i in range(0, times):     
        result = bio.generate(length) if not freq else bio.generate(length, freq)
        assert dna_regex.match(result)
        assert len(result) == length
        if freq:
            for b, fr in freq.items():
                if fr == 0:
                    assert b not in result

@pytest.mark.parametrize("seq, q", product(gen_seq(), (1, 2, 3, 4)))
def test_qunique_substring(seq, q):
    if len(seq) < q:
        with pytest.raises(AssertionError):
            bio.qunique_substring(seq, q)
    else:
        result = bio.qunique_substring(seq, q)
        assert len(result) <= len(seq)
        assert result in seq
        assert bio.check_q_uniqueness(result, q)[0]

@pytest.mark.parametrize("strat, args, seq", product(["replace", "delete", "nearest"],["A","G"]\
                                                     ,gen_seq() + ["A4zCa", "3247", "\nAtt\t"]) ) 
def test_strategy(strat, args, seq):
    valid_base = ['A', 'T', 'C', 'G', 'a', 't', 'g', 'c']
    f = bio.strategy(strat, args)
    result = f(seq)
    if strat == "delete":
        assert len(seq) >= len(result)

    elif strat == "replace" or strat == "nearest":
        assert len(seq) == len(result)
        for i in range(0,len(seq)):
            if seq[i] in valid_base:
                assert result[i] == seq[i].upper()
            elif strat == "nearest":
                assert abs(ord(result[i]) - ord(seq[i].upper())) == min([abs( ord(seq[i].upper())-ord(c) )\
                                                                         for c in "ATGC"])
            elif strat == "replace":
                assert result[i] == args              
    
    assert dna_regex.match(result)

