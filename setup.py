from setuptools import setup

setup(name='bioinformatics',
      version='0.1',
      description='Python DNA Sequenz tools',
      author='Tobias Suttmann, Shimon Wonsak',
      author_email='tobias.suttmann@tu-dortmund.de, shimon.wonsak@tu-dortmund.de',
      license='TUD',
      url='nsaobservesyou.secret',
      packages=['BioFachprojekt'],
      install_requires=[
		'dinopy',
		'numpy'
	]
      #package_dir={'BioFachprojekt' : 'BioFachprojekt'}
     )
